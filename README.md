knlab-nixops
============

Everything to manage the lab's cloud compute resources, except the keys.
Mainly based on these:

* <https://hydra.nixos.org/build/115931128/download/1/manual/manual.html>
* <https://github.com/nh2/nixops-tutorial>
* <https://adrien-faure.fr/post/nixops-on-vultr/>

# Usage

```
git submodule update --init --recurisive
./mynix nixops create -d knlab knlab.nix
./mynix nixops deploy -d knlab
./mynix nixops check  -d knlab
./mynix nixops info   -d knlab
```

For some reason, you'll also need to export the key to your local `.ssh` dir and edit `.ssh/config` to match, like this:

TODO finish writing
