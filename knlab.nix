let
  ortholang      = import ./ortholang;
  ortholang-demo = import ./ortholang-demo;

in {
  network.description = "knlab test 1";
  network.enableRollback = true;

  vultrtest1 = { config, pkgs, ... }: {
    imports = [
      ./vultrtest1.nix
      ./ortholang-demo/service.nix
    ];
    networking.hostName = "vultrtest1";
    deployment.targetHost = "144.202.109.233";
    environment.systemPackages = with pkgs; [
      vim
    ];
    services.ortholangDemo = {
      enable      = true;
      tmpDir      = "/tmp/orthlang-demo";
      logPath     = "/tmp/orthlang-demo.log";
      usersDir    = "/tmp/orthlang-users";
      authPath    = "/tmp/orthlang-users/passwords.txt";
      examplesDir = builtins.toPath ./ortholang-demo/examples;
      commentsDir = "/tmp/ortholang-comments";
      user        = "root";
      port        = 80;
    };
  };

}
