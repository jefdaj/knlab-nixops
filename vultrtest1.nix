{ config, lib, pkgs, modulesPath, ... }:

{

  # from configuration.nix
  time.timeZone = "America/Los_Angeles";
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/vda";
  networking.useDHCP = false;
  networking.interfaces.ens3.useDHCP = true;
  services.openssh.enable = true;
  system.stateVersion = "20.09";

  # from hardware-configuration.nix
  imports =
    [ (modulesPath + "/profiles/qemu-guest.nix") # TODO will this work outside the vm?
    ];
  boot.initrd.availableKernelModules = [ "ata_piix" "uhci_hcd" "virtio_pci" "sr_mod" "virtio_blk" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];
  fileSystems."/" =
    { device = "/dev/disk/by-uuid/e46da7f9-0f3b-4262-880a-0762871b9fde";
      fsType = "ext4";
    };
  swapDevices =
    [ { device = "/dev/disk/by-uuid/384a05bb-4061-4b3a-a15a-50929ba1d7ec"; }
    ];

}
